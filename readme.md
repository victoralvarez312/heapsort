#**Heapsort Algorithm in Python & Go**#

Unit-tested heapsort implementation in Python and Go. Linearithmic runtime is validated using convergence of the [doubling ratio](http://algs4.cs.princeton.edu/14analysis/), which should converge to 2.

##**Requirements**##

* [Go](https://golang.org/doc/install)
* [Python](https://www.python.org/downloads/)

##**Getting Started**##

Clone the repository:

    $ git clone https://valvarez312@bitbucket.org/valvarez312/heapsort.git

`cd` into `heapsort`.

	$ cd heapsort

###**Python Heapsort**##

Unit tests:

	$ python python/heapsort/heap_test.py

Doubling ratio:

	$ python python/heapsort/heap_doubling_ratio.py

###**Go Heapsort**##

Unit tests:
	
	$ cd go/heapsort
	$ go test

Doubling ratio:
	
	$ cd go
	$ go run main.go

####**TODO:**####

* make python packages absolutely importable from project root
* figure out how to run `go test` from the project root
* incorporate [gensort](http://www.ordinal.com/gensort.html), figure out how to get `make` to succeed