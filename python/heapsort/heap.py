class Heap(object):
    '''
    Heap object used for in-place heapsort. 
    Consumes and modifies a list of comparable items.
    Once you instantiate the Heap with values, 
    values will be already be destroyed.

    Usage:

        > values = [int(100 * random.random()) for i in range(10)]
        > heap = Heap(values, ascending=False)
        > print values         # should now be heapified
        > heap.sort()          # sort values
        > print values         # should now be sorted

    API:
        heap.tree:
            - returns a view of the complete binary heap

        heap[i]:
            - returns a reference to the ith item in the tree (level-order)

        size:
            - the size of the heap, the same as the length of the input

        sort:
            - destructively sort the input

    Performance:

        Time:  O(N * log(N))
        Space: O(1)
    '''

    def __init__(self, items, ascending=True):
        self._ascending = ascending
        self._tree = items

        self._heapify()

    def __getitem__(self, i):
        return self._tree[i]

    def __setitem__(self, i, value):
        self._tree[i] = value

    @property
    def tree(self):
        return self._tree[:]

    @property
    def size(self):
        return len(self._tree)

    def _heapify(self):
        '''
        Turn self._tree into an ordered heap.

        Time:  O(N * log(N))
        Space: O(1)
        '''
        p = (self.size - 1)/2

        while p >= 0:
            self._sink(p)
            p -= 1

    def _sink(self, p, u=None):
        '''
        Iteratively perform a swap if the parent at
        index p is larger (resp. smaller) than the 
        smallest (resp. largest) of its children. 

        Time:  O(log(N))
        Space: O(1)
        '''
        if u is None: 
            u = self.size

        while 2*p + 1 < u:
            c = 2*p + 1

            l = self[c]
            r = self[c + 1] if (c + 1 < u) else None

            if r and self._ascending and (l < r):
                c += 1
            elif r and not self._ascending and (l > r):
                c += 1

            pr = self[p]
            ch = self[c]

            if (self._ascending and pr < ch) or (not self._ascending and pr > ch):
                self[p] = ch
                self[c] = pr

                p = c
            else:
                break

    def sort(self):
        j = self.size - 1

        while j > 0:
            root = self[0]

            self[0] = self[j]
            self[j] = root

            self._sink(0, u=j)

            j -= 1


if __name__ == '__main__':
    import random

    values = [int(100 * random.random()) for i in range(10)]
    print 'values: ', values

    heap = Heap(values, ascending=True)
    heap.sort()

    print 'ascending: ', values

    heap = Heap(values, ascending=False)
    heap.sort()

    print 'descending: ', values
