import unittest

from heap import Heap


class HeapTests(unittest.TestCase):

    def setUp(self):
        pass

    def test_heapifies_ascending(self):
        vals = [4, 3, 2, 7, 1, 9]
        tree = [9, 7, 4, 3, 1, 2]
        
        h = Heap(vals)

        self.assertEqual(h.tree, tree)

    def test_heapifies_descending(self):
        vals = [4, 3, 2, 7, 1, 9]
        tree = [1, 3, 2, 7, 4, 9]
        
        h = Heap(vals, ascending=False)

        self.assertEqual(h.tree, tree)

    def test_sorts_ascending(self):
        vals = [4, 3, 2, 7, 1, 9]
        
        h = Heap(vals)
        h.sort()

        self.assertEqual(vals, [1, 2, 3, 4, 7, 9])

    def test_sorts_descending(self):
        vals = [4, 3, 2, 7, 1, 9]
        
        h = Heap(vals, ascending=False)
        h.sort()

        self.assertEqual(vals, [9, 7, 4, 3, 2, 1])

    def test_sorts_comparables_ascending(self):
        vals = [char for char in 'string']
        
        h = Heap(vals)
        h.sort()

        self.assertEqual(vals, ['g', 'i', 'n', 'r', 's', 't'])

    def test_sorts_comparables_descending(self):
        vals = [char for char in 'string']
        
        h = Heap(vals, ascending=False)
        h.sort()

        self.assertEqual(vals, ['t', 's', 'r', 'n', 'i', 'g'])


if __name__ == '__main__':
    unittest.main()