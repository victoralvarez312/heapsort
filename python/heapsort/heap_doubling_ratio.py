import random
import time

from heap import Heap

MAX = 1e6

def time_trial(N):
    '''
    Return the time taken to sort N random integers from 0 to 1e6.
    '''
    start = time.time()

    vals = [int(MAX * random.random()) for i in range(N)]

    heap = Heap(vals)
    heap.sort()

    return time.time() - start

if __name__ == '__main__':
    '''
    The Doubling Ratio for these tests should converge 
    to ~2.0, implying a heapsort runtime of O(N * log(N)).
    '''

    N = 125
    prev = time_trial(N)

    print '     N   T(N)   Doubling Ratio'

    for j in range(10):
        N *= 2
        trial = time_trial(N)

        print '%6d %6.2f %6.2f' % (N, trial, trial/prev)

        prev = trial