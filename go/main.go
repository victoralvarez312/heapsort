package main

import (
    "./heapsort"
    "fmt"
    "math/rand"
    "time"
)

func TimeTrial(N int) float64 {
    start := time.Now()
    values := make([]int, N)

    for i := 0; i < N; i++ {
        values[i] = rand.Intn(1e6)
    }

    heapsort.Heapsort(values, true)

    return time.Since(start).Seconds()
}

func main() {
    N := 125
    prev := TimeTrial(N)

    fmt.Println("     N   T(N)   Doubling Ratio")

    for i := 0; i < 10; i++ {
        N *= 2
        trial := TimeTrial(N)

        fmt.Printf("%6d %6.2f %6.2f\n", N, trial, trial/prev)

        prev = trial
    }
}
