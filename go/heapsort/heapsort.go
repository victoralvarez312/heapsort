package heapsort

import "sort"

// modifies list in-place
// turns it into a binary heap
func heapify(data sort.Interface, ascending bool) {
    size := data.Len()

    for p := size - 1; p >= 0; p-- {
        sink(data, p, size, ascending)
    }
}

func sink(data sort.Interface, p int, u int, ascending bool) {
    for {
        c := 2*p + 1

        if c >= u {
            break
        }

        if c + 1 < u {
            if ascending && data.Less(c, c + 1) || !ascending && !data.Less(c, c + 1) {
                c++
            }
        }

        if ascending && data.Less(p, c) || !ascending && !data.Less(p, c) {
            data.Swap(p, c)

            p = c
        } else {
            break
        }
    }
}

// Heapsort an array of integers
func Heapsort(list []int, ascending bool) {
    data := sort.IntSlice(list)
    heapify(data, ascending)

    for j := data.Len() - 1; j > 0; j-- {
        data.Swap(0, j)
        sink(data, 0, j, ascending)
    }
}
