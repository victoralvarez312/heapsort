package heapsort

import (
    "sort"
    "testing"
)

func TestMaxHeapify(t *testing.T) {
    ascending := true
    l := 6

    vals := sort.IntSlice([]int{4, 3, 2, 7, 1, 9})
    heap := []int{9, 7, 4, 3, 1, 2}

    heapify(vals, ascending)

    for i := 0; i < l; i++ {
        if vals[i] != heap[i] {
            t.Error("Max-heapify failed; expected", heap[i], "got", vals[i])
            t.FailNow()
        }
    }

    t.Log("Max-heapify passed.")
}

func TestHeapsortAscending(t *testing.T) {
    ascending := true
    l := 6

    values := []int{4, 3, 2, 7, 1, 9}
    sorted := []int{1, 2, 3, 4, 7, 9}

    Heapsort(values, ascending)

    for i := 0; i < l; i++ {
        if values[i] != sorted[i] {
            t.Error("Heapsort failed; expected", sorted[i], "got", values[i])
            t.FailNow()
        }
    }

    t.Log("Heapsort passed.")
}

func TestMinHeapify(t *testing.T) {
    ascending := false
    l := 6

    vals := sort.IntSlice([]int{4, 3, 2, 7, 1, 9})
    heap := []int{1, 3, 2, 7, 4, 9}

    heapify(vals, ascending)

    for i := 0; i < l; i++ {
        if vals[i] != heap[i] {
            t.Error("Min-heapify failed; expected", heap[i], "got", vals[i])
            t.FailNow()
        }
    }

    t.Log("Min-heapify passed.")
}

func TestHeapsortDescending(t *testing.T) {
    ascending := false
    l := 6

    values := []int{4, 3, 2, 7, 1, 9}
    sorted := []int{9, 7, 4, 3, 2, 1}

    Heapsort(values, ascending)

    for i := 0; i < l; i++ {
        if values[i] != sorted[i] {
            t.Error("Reverse Heapsort failed; expected", sorted[i], "got", values[i])
            t.FailNow()
        }
    }

    t.Log("Reverse Heapsort passed.")
}
